# TheOfficeDataVisualization

The Office data visualization is a data visualization project based on the American version of popular series [The Office](https://www.imdb.com/title/tt0386676/). The purpose of this is to visualize the facts about this tv-series. Here you can perceive: how long took to film each season, seasons with its number of episodes, average rating and average viewership for each season. Also you can find out rating, viewership (US only) and duration for every episode. Top rated and the most viewed episodes of the series are visualized as a list. See who took the main word in this mockumentary in Total words spoken by main characters stacked bar chart. And lastly you can compare the number of lines spoken by main characters by season.
            
To visit the site, click [here](https://the-office-data-visualization.herokuapp.com/).

Technologies used: HTML, CSS, Bootstrap, JavaScript, D3.js
