let allEpisodes = null
let allLines = null
let tooltip


$(document).ready(function () {
    $('#chartBox').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        arrows: false
    });
    $.when($.getJSON("/data/the-office-series.json", function (data) {
        allEpisodes = data
    })).then(function () {
        $("#seasonsSelect").on("change", function () {
            drawSeasonRatingsBarChart(getSeason($("#seasonsSelect").val()))
        })
        $("#btn_showRatings").click(function () {
            drawSeasonRatingsBarChart(getSeason($("#seasonsSelect").val()))
        })
        $("#btn_showViewership").click(function () {
            drawSeasonViewerShipBarChart(getSeason($("#seasonsSelect").val()))
        })
        $("#btn_showRatingsScatterPlot").click(function () {
            drawAllEpisodesRatingsScatterPlot()
        })
        $("#btn_showViewershipScatterPlot").click(function () {
            drawAllEpisodesViewershipScatterPlot()
        })
        $("#btn_showAvgRatings").click(function () {
            drawAvgRatingPerSeasonBarChart()
        })
        $("#btn_showAvgViewership").click(function () {
            drawAvgViewershipPerSeasonBarChart()
        })
        $("#charactersBox div").click(function (e) {
            selectCharacter(e.currentTarget.id)
        })

        drawTimeline()
        drawSeasonsPieChart()
        appendSeasonSelection()
        drawSeasonRatingsBarChart(getSeason(1))
        drawTopRatedEpisodes(10)
        drawMostPopularEpisodes(10)
        drawAvgRatingPerSeasonBarChart()
        drawWordsSpokenBarChart()
        drawAllEpisodesRatingsScatterPlot()
        selectInitialTwoCharacters()
        loadTheOfficeLinesJSON()
        tooltip = d3.select("body").append("div").attr("id", "tooltip").attr("class", "hidden")

    })
})

function appendSeasonSelection() {
    let html = ''
    let i
    for (i = 1; i <= allEpisodes[allEpisodes.length - 1].Season; i++) {
        html += '<option value="' + i + '">Season ' + i + '</option>'
    }
    $("#seasonsSelect").append(html)
}

/* Functions for drawing charts */
function drawTimeline() {
    google.charts.load("current", { packages: ["timeline"] })
    google.charts.setOnLoadCallback(drawChart)
    function drawChart() {
        var container = document.getElementById('timeline')
        var chart = new google.visualization.Timeline(container)
        var dataTable = new google.visualization.DataTable()
        dataTable.addColumn({ type: 'string', id: 'Seasons' })
        dataTable.addColumn({ type: 'string', id: 'Name' })
        dataTable.addColumn({ type: 'date', id: 'Start' })
        dataTable.addColumn({ type: 'date', id: 'End' })
        dataTable.addColumn({ type: 'string', 'role': 'tooltip', 'p': { 'html': true } })
        let i
        let dateFormat = [{ day: 'numeric' }, { month: 'short' }, { year: 'numeric' }]
        var color = d3.scaleOrdinal(d3.schemeCategory10)
        var colors = []
        for (i = 1; i <= 9; i++) {
            dataTable.addRow(['Seasons', 'Season ' + i, new Date(getSeason(i)[0].Date), new Date(getSeason(i)[getSeason(i).length - 1].Date), "Season " + i + "<br>" + join(new Date(getSeason(i)[0].Date), dateFormat, " ") + " - " + join(new Date(getSeason(i)[getSeason(i).length - 1].Date), dateFormat, " ")])
            colors.push(color(i))
        }
        var options = {
            hAxis: {
                format: "MMM Y"
            },
            colors: colors
        }
        chart.draw(dataTable, options)
        function myHandler(e) {
            if (e.row != null) {
                $(".google-visualization-tooltip").html(dataTable.getValue(e.row, 4)).css({ width: "auto", height: "auto", "font-size": "0.7em" })
            }
        }
        google.visualization.events.addListener(chart, 'onmouseover', myHandler)
    }
}

function drawSeasonsPieChart() {
    let data = []
    let i
    for (i = 1; i <= 9; i++) {
        let season = { season: "Season " + i, numberOfEpisodes: getNumberOfEpisodes(getSeason(i)), episodes: getSeason(i) }
        data.push(season)
    }

    var width = 770
    var height = 500
    var margin = 50
    var outerRadius = Math.min(width, height) / 2 - margin
    var innerRadius = 0
    var color = d3.scaleOrdinal(d3.schemeCategory10)
    var arc = d3.arc()
        .innerRadius(innerRadius)
        .outerRadius(outerRadius)
    var pie = d3.pie()
        .value(function (d) { return d.numberOfEpisodes })
        .sort(function (a, b) { return d3.ascending(a.season, b.season) })
    var svg = d3.select("#seasonsPieChart")
        .append("svg")
        .attr("preserveAspectRatio", "xMinYMin meet")
        .attr("viewBox", "-250 -20 1300 500")
    var pieArcs = svg.selectAll("g.pie")
        .data(pie(data))
        .enter()
        .append("g")
        .attr("class", "pie")
        .attr("transform", "translate(" + (width / 2) + ", " + (height / 2) + ")")
    pieArcs.append("path")
        .attr("fill", function (d, i) { return color(i) })
        .attr("stroke", "black")
        .attr("stroke-width", "1")
        .attr("d", arc)
    pieArcs.append("text")
        .attr("transform", function (d) {
            var c = arc.centroid(d),
                x = c[0],
                y = c[1],
                // pythagorean theorem for hypotenuse
                h = Math.sqrt(x * x + y * y)
            var radius = outerRadius + 30
            return "translate(" + (x / h * radius) + ',' + (y / h * radius) + ")"
        }).attr("text-anchor", function (d) {
            var midAngle = (d.startAngle + d.endAngle) / 2
            if (midAngle <= 180 * Math.PI / 180 && midAngle >= 25 * Math.PI / 180) {
                return "start"
            }
            else if (midAngle >= 180 * Math.PI / 180 && midAngle <= 360 * Math.PI / 180) {
                return "end"
            }
            return "middle"
        }).text(function (d) { return d.data.season })

    pieArcs.on("mouseover", function (d) {
        tooltip
            .style("left", d3.event.pageX + "px")
            .style("top", d3.event.pageY + "px")
            .style("opacity", 1)
            .style("font-family", "sans-serif")
            .html(`${d.data.season}<br>Number of episodes: ${d.data.episodes.length}<br>${d.data.episodes[0].Date} - ${d.data.episodes[d.data.episodes.length - 1].Date}`)
    })
        .on("mouseout", function () {
            // Hide the tooltip
            tooltip.style("opacity", 0)
        })
}

function drawSeasonRatingsBarChart(season) {
    d3.select("#seasonEpisodesBarChart").html("")

    var color = d3.scaleOrdinal(d3.schemeCategory10)
    //function(d) {return color(d.Season)} doesnt work
    var i, colors = []
    for (i = 0; i < 10; i++) {
        colors.push(color(i))
    }

    // set the dimensions and margins of the graph
    var margin = { top: 80, right: 30, bottom: 250, left: 80 },
        width = 700 - margin.left - margin.right,
        height = 720 - margin.top - margin.bottom

    // append the svg object to the body of the page
    var svg = d3.select("#seasonEpisodesBarChart")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("length", "auto")
        .attr("viewBox", "0 0 " + 700 + " " + 720)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")")

    // X axis
    var x = d3.scaleBand()
        .range([0, width])
        .domain(season.map(function (d) { return d.EpisodeTitle }))
        .padding(0.2)
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x))
        .selectAll("text")
        .attr("transform", "translate(-10,0)rotate(-45)")
        .style("text-anchor", "end")
        .style("font-size", "2em")

    // Add Y axis
    var y = d3.scaleLinear()
        .domain([0, 10])
        .range([height, 0])
    svg.append("g")
        .call(d3.axisLeft(y))

    svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("x", 0 - (height / 2))
        .attr("y", 0 - (margin.left / 2))
        .style("text-anchor", "middle")
        .text("Ratings")
        .style("font-size", "2rem")

    svg.selectAll("rect")
        .data(season)
        .enter()
        .append("rect")
        .attr("x", function (d) { return x(d.EpisodeTitle) })
        .attr("y", function (d) { return y(0) })
        .attr("width", x.bandwidth())
        .attr("height", function (d) { return height - y(0) })
        .attr("fill", function (d) { return colors[d.Season - 1] })

    svg.selectAll("rect")
        .transition()
        .duration(700)
        .attr("y", function (d) { return y(d.Ratings) })
        .attr("height", function (d) { return height - y(d.Ratings) })
        .delay(function (d, i) { return (i * 50) })

    svg.append("text")
        .attr("x", (width / 2))
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")
        .style("font-size", "1.5em")
        .text("Rating per episode - Season " + season[0].Season)
        .style("font-size", "2.1rem")

    svg.selectAll("rect").on("mouseover", function (d) {
        tooltip
            .style("left", d3.event.pageX + "px")
            .style("top", d3.event.pageY + "px")
            .style("opacity", 1)
            .style("font-family", "sans-serif")
            .html(`${d.EpisodeTitle}<br>Duration: ${d.Duration} min<br>Rating: ${d.Ratings}<br>Viewership: ${d.Viewership} millions`)
    })
        .on("mouseout", function () {
            // Hide the tooltip
            tooltip.style("opacity", 0)
        })
}

function drawSeasonViewerShipBarChart(season) {
    d3.select("#seasonEpisodesBarChart").html("")

    var color = d3.scaleOrdinal(d3.schemeCategory10)
    //function(d) {return color(d.Season)} doesnt work
    var i, colors = []
    for (i = 0; i < 10; i++) {
        colors.push(color(i))
    }

    // set the dimensions and margins of the graph
    var margin = { top: 80, right: 30, bottom: 250, left: 80 },
        width = 700 - margin.left - margin.right,
        height = 720 - margin.top - margin.bottom

    // append the svg object to the body of the page
    var svg = d3.select("#seasonEpisodesBarChart")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("length", "auto")
        .attr("viewBox", "0 0 " + 700 + " " + 720)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")")

    // X axis
    var x = d3.scaleBand()
        .range([0, width])
        .domain(season.map(function (d) { return d.EpisodeTitle }))
        .padding(0.2)
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x))
        .selectAll("text")
        .attr("transform", "translate(-10,0)rotate(-45)")
        .style("text-anchor", "end")
        .style("font-size", "2em")

    // Add Y axis
    var y = d3.scaleLinear()
        .domain([0, Math.max(...season.map(function (d) { return d.Viewership }))])
        .range([height, 0])
    svg.append("g")
        .call(d3.axisLeft(y))

    svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("x", 0 - (height / 2))
        .attr("y", 0 - (margin.left / 2))
        .style("text-anchor", "middle")
        .text("Viewership [milions]")
        .style("font-size", "2rem")

    svg.selectAll("rect")
        .data(season)
        .enter()
        .append("rect")
        .attr("x", function (d) { return x(d.EpisodeTitle) })
        .attr("y", function (d) { return y(0) })
        .attr("width", x.bandwidth())
        .attr("height", function (d) { return height - y(0) })
        .attr("fill", function (d) { return colors[d.Season - 1] })

    svg.selectAll("rect")
        .transition()
        .duration(700)
        .attr("y", function (d) { return y(d.Viewership) })
        .attr("height", function (d) { return height - y(d.Viewership) })
        .delay(function (d, i) { return (i * 50) })

    svg.append("text")
        .attr("x", (width / 2))
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")
        .text("Viewership in US per episode - Season " + season[0].Season)
        .style("font-size", "2.1rem")

    svg.selectAll("rect").on("mouseover", function (d) {
        tooltip
            .style("left", d3.event.pageX + "px")
            .style("top", d3.event.pageY + "px")
            .style("opacity", 1)
            .style("font-family", "sans-serif")
            .html(`${d.EpisodeTitle}<br>Duration: ${d.Duration} min<br>Rating: ${d.Ratings}<br>Viewership: ${d.Viewership} millions`)
    })
        .on("mouseout", function () {
            // Hide the tooltip
            tooltip.style("opacity", 0)
        })
}

function drawAvgRatingPerSeasonBarChart() {
    let data = []
    let i
    for (i = 1; i <= 9; i++) {
        let season = { season: "Season " + i, numberOfEpisodes: getNumberOfEpisodes(getSeason(i)), episodes: getSeason(i), avgRating: getAverageRating(getSeason(i)), avgViewership: getAverageViewership(getSeason(i)) }
        data.push(season)
    }

    d3.select("#wholeSeriesBarChart").html("")
    var color = d3.scaleOrdinal(d3.schemeCategory10)

    // set the dimensions and margins of the graph
    var margin = { top: 80, right: 30, bottom: 80, left: 80 },
        width = 700 - margin.left - margin.right,
        height = 550 - margin.top - margin.bottom

    // append the svg object to the body of the page
    var svg = d3.select("#wholeSeriesBarChart")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("length", "auto")
        .attr("viewBox", "0 0 " + 700 + " " + 550)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")")

    // X axis
    var x = d3.scaleBand()
        .range([0, width])
        .domain(data.map(function (d) { return d.season }))
        .padding(0.2)
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x))
        .selectAll("text")
        .attr("transform", "translate(-10,0)rotate(-45)")
        .style("text-anchor", "end")
        .style("font-size", "2em")

    // Add Y axis
    var y = d3.scaleLinear()
        .domain([0, 10])
        .range([height, 0])
    svg.append("g")
        .call(d3.axisLeft(y))

    svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("x", 0 - (height / 2))
        .attr("y", 0 - (margin.left / 2))
        .style("text-anchor", "middle")
        .text("Ratings")
        .style("font-size", "2rem")

    svg.selectAll("rect")
        .data(data)
        .enter()
        .append("rect")
        .attr("x", function (d) { return x(d.season) })
        .attr("y", function (d) { return y(0) })
        .attr("width", x.bandwidth())
        .attr("height", function (d) { return height - y(0) })
        .attr("fill", function (d, i) { return color(i) })

    svg.selectAll("rect")
        .transition()
        .duration(700)
        .attr("y", function (d) { return y(d.avgRating) })
        .attr("height", function (d) { return height - y(d.avgRating) })
        .delay(function (d, i) { return (i * 50) })

    svg.append("text")
        .attr("x", (width / 2))
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")
        .text("Average rating per season")
        .style("font-size", "2.1rem")

    svg.selectAll("rect").on("mouseover", function (d) {
        tooltip
            .style("left", d3.event.pageX + "px")
            .style("top", d3.event.pageY + "px")
            .style("opacity", 1)
            .style("font-family", "sans-serif")
            .html(`${d.season}<br>Average rating: ${d.avgRating.toFixed(2)}/10<br>Average viewership: ${d.avgViewership.toFixed(2)} milions`)
    })
        .on("mouseout", function () {
            // Hide the tooltip
            tooltip.style("opacity", 0)
        })
}

function drawAvgViewershipPerSeasonBarChart() {
    let data = []
    let i
    for (i = 1; i <= 9; i++) {
        let season = { season: "Season " + i, numberOfEpisodes: getNumberOfEpisodes(getSeason(i)), episodes: getSeason(i), avgRating: getAverageRating(getSeason(i)), avgViewership: getAverageViewership(getSeason(i)) }
        data.push(season)
    }

    d3.select("#wholeSeriesBarChart").html("")
    var color = d3.scaleOrdinal(d3.schemeCategory10)

    // set the dimensions and margins of the graph
    var margin = { top: 80, right: 30, bottom: 80, left: 80 },
        width = 700 - margin.left - margin.right,
        height = 550 - margin.top - margin.bottom

    // append the svg object to the body of the page
    var svg = d3.select("#wholeSeriesBarChart")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("length", "auto")
        .attr("viewBox", "0 0 " + 700 + " " + 550)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")")

    // X axis
    var x = d3.scaleBand()
        .range([0, width])
        .domain(data.map(function (d) { return d.season }))
        .padding(0.2)
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x))
        .selectAll("text")
        .attr("transform", "translate(-10,0)rotate(-45)")
        .style("text-anchor", "end")
        .style("font-size", "2em")

    // Add Y axis
    var y = d3.scaleLinear()
        .domain([0, Math.ceil(Math.max(...data.map(function (d) { return d.avgViewership })))])
        .range([height, 0])
    svg.append("g")
        .call(d3.axisLeft(y))

    svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("x", 0 - (height / 2))
        .attr("y", 0 - (margin.left / 2))
        .style("text-anchor", "middle")
        .text("Viewership [milions]")
        .style("font-size", "2rem")

    svg.selectAll("rect")
        .data(data)
        .enter()
        .append("rect")
        .attr("x", function (d) { return x(d.season) })
        .attr("y", function (d) { return y(0) })
        .attr("width", x.bandwidth())
        .attr("height", function (d) { return height - y(0) })
        .attr("fill", function (d, i) { return color(i) })

    svg.selectAll("rect")
        .transition()
        .duration(700)
        .attr("y", function (d) { return y(d.avgViewership) })
        .attr("height", function (d) { return height - y(d.avgViewership) })
        .delay(function (d, i) { return (i * 50) })

    svg.append("text")
        .attr("x", (width / 2))
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")
        .text("Average viewership per season in US")
        .style("font-size", "2.1rem")

    svg.selectAll("rect").on("mouseover", function (d) {
        tooltip
            .style("left", d3.event.pageX + "px")
            .style("top", d3.event.pageY + "px")
            .style("opacity", 1)
            .style("font-family", "sans-serif")
            .html(`${d.season}<br>Average rating: ${d.avgRating.toFixed(2)}/10<br>Average viewership: ${d.avgViewership.toFixed(2)} milions`)
    })
        .on("mouseout", function () {
            // Hide the tooltip
            tooltip.style("opacity", 0)
        })
}

function drawAllEpisodesRatingsScatterPlot() {
    d3.select("#allEpisodesScatterPlot").html("")
    var color = d3.scaleOrdinal(d3.schemeCategory10)
    // set the dimensions and margins of the graph
    var margin = { top: 80, right: 30, bottom: 80, left: 110 },
        width = 900 - margin.left - margin.right,
        height = 650 - margin.top - margin.bottom

    // append the svg object to the body of the page
    var svg = d3.select("#allEpisodesScatterPlot")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("length", "auto")
        .attr("viewBox", "30 0 " + 900 + " " + 650)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")")

    // Add the grey background that makes ggplot2 famous
    svg
        .append("rect")
        .attr("x", 0)
        .attr("y", 0)
        .attr("height", height)
        .attr("width", width)
        .style("fill", "black")

    // Add X axis
    var x = d3.scaleTime()
        .domain([new Date(allEpisodes[0].Date), new Date(allEpisodes[allEpisodes.length - 1].Date)])
        .range([0, width])
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x).ticks(10))
        .selectAll("text")
        .attr("transform", "translate(-10,0)")
        .style("text-anchor", "middle")
        .style("font-size", "2em")

    // Add Y axis
    var y = d3.scaleLinear()
        .domain([0, 10])
        .range([height, 0])
        .nice()
    svg.append("g")
        .call(d3.axisLeft(y).tickSize(-width).ticks(10))

    // Customization
    svg.selectAll(".tick line").attr("stroke", "white")

    svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("x", 0 - (height / 2))
        .attr("y", 0 - (margin.left / 2.5))
        .style("text-anchor", "middle")
        .text("Ratings")
        .style("font-size", "2rem")

    svg.append("text")
        .attr("x", (width / 2))
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")
        .text("Rating per episode")
        .style("font-size", "2.5rem")


    // Add dots
    svg.append("g")
        .selectAll("dot")
        .data(allEpisodes)
        .enter()
        .append("circle")
        .attr("cx", function (d, i) { return x(new Date(d.Date)) })
        .attr("cy", function (d, i) { return y(d.Ratings) })
        .attr("r", 0)
        .style("fill", function (d) { return color(d.Season) })
        .on("mouseover", function (d) {
            d3.select(this).transition().duration(100).attr("r", "7")
            tooltip
                .style("left", d3.event.pageX + "px")
                .style("top", d3.event.pageY + "px")
                .style("opacity", 1)
                .style("font-family", "sans-serif")
                .html(`${d.EpisodeTitle}<br>Duration: ${d.Duration} min<br>Rating: ${d.Ratings}<br>Viewership: ${d.Viewership} millions`)
        })
        .on("mouseout", function () {
            // Hide the tooltip
            d3.select(this).transition().duration(100).attr("r", "5")
            tooltip.style("opacity", 0)
        })

    svg.selectAll("circle").transition().duration(500).attr("r", 5).delay(function (d, i) { return (i * 5) })
}

function drawAllEpisodesViewershipScatterPlot() {
    d3.select("#allEpisodesScatterPlot").html("")
    var color = d3.scaleOrdinal(d3.schemeCategory10)
    // set the dimensions and margins of the graph
    var margin = { top: 80, right: 30, bottom: 80, left: 110 },
        width = 900 - margin.left - margin.right,
        height = 650 - margin.top - margin.bottom

    // append the svg object to the body of the page
    var svg = d3.select("#allEpisodesScatterPlot")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("length", "auto")
        .attr("viewBox", "30 0 " + 900 + " " + 650)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")")

    // Add the grey background that makes ggplot2 famous
    svg
        .append("rect")
        .attr("x", 0)
        .attr("y", 0)
        .attr("height", height)
        .attr("width", width)
        .style("fill", "black")

    // Add X axis
    var x = d3.scaleTime()
        .domain([new Date(allEpisodes[0].Date), new Date(allEpisodes[allEpisodes.length - 1].Date)])
        .range([0, width])
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x).ticks(10))
        .selectAll("text")
        .attr("transform", "translate(-10,0)")
        .style("text-anchor", "middle")
        .style("font-size", "2em")

    // Add Y axis
    var y = d3.scaleLinear()
        .domain([0, getMostViewedEpisodes(1)[0].Viewership])
        .range([height, 0])
        .nice()
    svg.append("g")
        .call(d3.axisLeft(y).tickSize(-width).ticks(10))

    // Customization
    svg.selectAll(".tick line").attr("stroke", "white")

    svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("x", 0 - (height / 2))
        .attr("y", 0 - (margin.left / 2.5))
        .style("text-anchor", "middle")
        .text("Viewership [millions]")
        .style("font-size", "2rem")

    svg.append("text")
        .attr("x", (width / 2))
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")
        .text("Viewership in US per episode")
        .style("font-size", "2.5rem")

    // Add dots
    svg.append("g")
        .selectAll("dot")
        .data(allEpisodes)
        .enter()
        .append("circle")
        .attr("cx", function (d, i) { return x(new Date(d.Date)) })
        .attr("cy", function (d, i) { return y(d.Viewership) })
        .attr("r", 0)
        .style("fill", function (d) { return color(d.Season) })
        .on("mouseover", function (d) {
            d3.select(this).transition().duration(100).attr("r", "7")
            tooltip
                .style("left", d3.event.pageX + "px")
                .style("top", d3.event.pageY + "px")
                .style("opacity", 1)
                .style("font-family", "sans-serif")
                .html(`${d.EpisodeTitle}<br>Duration: ${d.Duration} min<br>Rating: ${d.Ratings}<br>Viewership: ${d.Viewership} millions`)
        })
        .on("mouseout", function () {
            // Hide the tooltip
            d3.select(this).transition().duration(100).attr("r", "5")
            tooltip.style("opacity", 0)
        })

    svg.selectAll("circle").transition().duration(500).attr("r", 5).delay(function (d, i) { return (i * 5) })
}

function drawTopRatedEpisodes(numberOfEpisodes) {
    let episodes = getTopRatedEpisodes(numberOfEpisodes)

    var color = d3.scaleLinear()
        .domain([3, numberOfEpisodes - 1])
        .range(['#795548', '#8D6E63'])

    d3.select("#ol_topRatedEpisodes").selectAll("li").data(episodes).enter().append("li").html(function (d) { return d.EpisodeTitle })
    d3.select("#ol_topRatedEpisodes").selectAll("li").style("background-color", function (data, index) {
        switch (index) {
            case 0: return "gold"
            case 1: return "silver"
            case 2: return "#cd7f32"
            default: return color(index)
        }
    })
        .style("font-weight", function (data, index) {
            if (index == 0) {
                return "bold"
            }
        })

    d3.select("#ol_topRatedEpisodes").selectAll("li").on("mouseover", function (d) {
        tooltip
            .style("z-index", 1000)
            .style("left", d3.event.pageX + "px")
            .style("top", d3.event.pageY + "px")
            .style("opacity", 1)
            .style("font-family", "sans-serif")
            .html(`Season: ${d.Season}<br>Duration: ${d.Duration} min<br>Rating: ${d.Ratings}<br>Viewership: ${d.Viewership} millions`)
    })
        .on("mouseout", function () {
            // Hide the tooltip
            tooltip.style("opacity", 0)
        })
}

function drawMostPopularEpisodes(numberOfEpisodes) {
    let episodes = getMostViewedEpisodes(numberOfEpisodes)

    var color = d3.scaleLinear()
        .domain([3, numberOfEpisodes - 1])
        .range(['#795548', '#8D6E63'])

    d3.select("#ol_mostPopularEpisodes").selectAll("li").data(episodes).enter().append("li").html(function (d) { return d.EpisodeTitle })
    d3.select("#ol_mostPopularEpisodes").selectAll("li").style("background-color", function (data, index) {
        switch (index) {
            case 0: return "gold"
            case 1: return "silver"
            case 2: return "#cd7f32"
            default: return color(index)
        }
    })
        .style("font-weight", function (data, index) {
            if (index == 0) {
                return "bold"
            }
        })

    d3.select("#ol_mostPopularEpisodes").selectAll("li").on("mouseover", function (d) {
        tooltip
            .style("z-index", 1000)
            .style("left", d3.event.pageX + "px")
            .style("top", d3.event.pageY + "px")
            .style("opacity", 1)
            .style("font-family", "sans-serif")
            .html(`Season: ${d.Season}<br>Duration: ${d.Duration} min<br>Rating: ${d.Ratings}<br>Viewership: ${d.Viewership} millions`)
    })
        .on("mouseout", function () {
            // Hide the tooltip
            tooltip.style("opacity", 0)
        })
}

function drawWordsSpokenBarChart() {
    d3.csv("/data/words-spoken.csv", function (data) {
        var color = d3.scaleOrdinal(d3.schemeCategory10)
        // set the dimensions and margins of the graph
        var margin = { top: 80, right: 30, bottom: 80, left: 200 },
            width = 820 - margin.left - margin.right,
            height = 650 - margin.top - margin.bottom
        // append the svg object to the body of the page
        var svg = d3.select("#wordsSpokenBarChart")
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("length", "auto")
            .attr("viewBox", "60 0 " + 750 + " " + 650)
            .append("g")
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")")
        var subgroups = data.columns.slice(1)
        var groups = d3.map(data, function (d) { return (d.group) }).keys()

        // Add X axis
        var x = d3.scaleBand()
            .domain(groups)
            .range([0, width])
            .padding([0.2])
        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x))
            .selectAll("text")
            .attr("transform", "translate(-10,0)rotate(-45)")
            .style("text-anchor", "end")
            .style("font-size", "2em")
        // Add Y axis
        var y = d3.scaleLinear()
            .domain([0, 160000])
            .range([height, 0])
        svg.append("g")
            .call(d3.axisLeft(y))

        svg.append("text")
            .attr("transform", "rotate(-90)")
            .attr("x", 0 - (height / 2))
            .attr("y", 0 - (margin.left / 2))
            .style("text-anchor", "middle")
            .text("Words spoken")
            .style("font-size", "2rem")

        //stack the data --> stack per subgroup
        var stackedData = d3.stack()
            .keys(subgroups)
            (data)

        svg.append("g")
            .selectAll("g")
            .data(stackedData)
            .enter().append("g")
            .attr("fill", function (d, i) { return color(i) })
            .selectAll("rect")
            .data(function (d) { return d })
            .enter().append("rect")
            .attr("x", function (d) { return x(d.data.group) })
            .attr("y", function (d) { return y(d[1]) })
            .attr("height", function (d) { return y(d[0]) - y(d[1]) })
            .attr("width", x.bandwidth())

        svg.append("text")
            .attr("x", (width / 2.5))
            .attr("y", 0 - (margin.top / 2))
            .attr("text-anchor", "middle")
            .text("Total words spoken by main characters")
            .style("font-size", "2.5rem")

        // Legend
        for (let i = 0; i < 9; i++) {
            svg.append("circle").attr("cx", width / 1.25).attr("cy", 50 + i * 25).attr("r", 7).style("fill", color(i))
            svg.append("text").attr("x", width / 1.25 + 15).attr("y", 50 + i * 25).text(`Season ${i + 1}`).style("font-size", "1rem").attr("alignment-baseline", "middle")
        }

        svg.selectAll("rect").on("mouseover", function (d) {
            tooltip
                .style("left", d3.event.pageX + "px")
                .style("top", d3.event.pageY + "px")
                .style("opacity", 1)
                .style("font-family", "sans-serif")
                .style("z-index", 1000)
                .html(`${d.data.group}<br>${getKeyByValue(d.data, `${d[1] - d[0]}`)} words spoken: ${d[1] - d[0]}<br>Total words spoken: ${getTotalWordsSpoken(d.data)}`)
        })
            .on("mouseout", function () {
                // Hide the tooltip
                tooltip.style("opacity", 0)
            })
    })
}

let firstCharacterBoxId
let secondCharacterBoxId
function selectInitialTwoCharacters() {
    firstCharacterBoxId = "michaelBox"
    $(`#${firstCharacterBoxId}`).css("border", "0.3rem solid red")
    secondCharacterBoxId = "dwightBox"
    $(`#${secondCharacterBoxId}`).css("border", "0.3rem solid blue")
    $("#firstCharacter").prop('checked', true)
}

function loadTheOfficeLinesJSON() {
    if (allLines == null) {
        d3.json("data/the_office_lines.json", function (data) {
            allLines = data
            drawLinesByCharacterComparisonChart()
        })
    }
    else { drawLinesByCharacterComparisonChart() }
}

function drawLinesByCharacterComparisonChart() {
    let graphData = getLinesByCharacterData()

    d3.select("#linesByCharacterChart").html("")
    var color = d3.scaleOrdinal(["#FF0000", "#0000FF"])
    // set the dimensions and margins of the graph
    var margin = { top: 80, right: 100, bottom: 80, left: 140 },
        width = 700 - margin.left - margin.right,
        height = 550 - margin.top - margin.bottom
    // append the svg object to the body of the page
    var svg = d3.select("#linesByCharacterChart")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("length", "auto")
        .attr("viewBox", "0 0 " + 700 + " " + 550)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")")
    // X axis
    var x = d3.scaleBand()
        .range([0, width])
        .domain(graphData.map(function (d) { return d.season }))
        .padding(0.2)
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x))
        .selectAll("text")
        .attr("transform", "translate(-10,0)rotate(-45)")
        .style("text-anchor", "end")
        .style("font-size", "2em")
    // Add Y axis
    let firstCharacterName = $(`#${firstCharacterBoxId} p`).html()
    let secondCharacterName = $(`#${secondCharacterBoxId} p`).html()
    let firstCharacterMax = Math.max.apply(Math, graphData.map(function (o) { return o[firstCharacterName] }))
    let secondCharacterMax = Math.max.apply(Math, graphData.map(function (o) { return o[secondCharacterName] }))
    let yAxisMaxValue = (firstCharacterMax > secondCharacterMax) ? firstCharacterMax : secondCharacterMax
    var y = d3.scaleLinear()
        .domain([0, yAxisMaxValue])
        .range([height, 0])
    svg.append("g")
        .call(d3.axisLeft(y))
    svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("x", 0 - (height / 2))
        .attr("y", 0 - (margin.left / 2))
        .style("text-anchor", "middle")
        .text("Lines")
        .style("font-size", "2rem")

    svg.append("text")
        .attr("x", (width / 2))
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")
        .text("Lines by character per season")
        .style("font-size", "2.5rem")

    var charactersNumOfLines = d3.keys(graphData[0]).filter(function (key) { return key !== "season" })
    graphData.forEach(function (d) {
        d.lines = charactersNumOfLines.map(function (name) {
            return { name: name, value: +d[name] }
        })
    })

    var x1 = d3.scaleBand().domain(charactersNumOfLines).range([0, x.bandwidth()])

    var season = svg.selectAll(".groups")
        .data(graphData)
        .enter().append("g")
        .attr("transform", function (d) { return "translate(" + x(d.season) + ",0)"; });

    season.selectAll("rect")
        .data(function (d) { return d.lines; })
        .enter()
        .append("rect")
        .attr("width", x1.bandwidth())
        .attr("x", function (d) { return x1(d.name); })
        .attr("y", function (d) { return y(0); })
        .attr("height", function (d) { return height - y(0); })
        .style("fill", function (d) { return color(d.name); });

    svg.selectAll("rect")
        .transition()
        .duration(700)
        .attr("y", function (d) { return y(d.value) })
        .attr("height", function (d) { return height - y(d.value) })
        .delay(function (d, i) { return (i * 50) })

    svg.selectAll("rect").on("mouseover", function (d) {
        tooltip
            .style("left", d3.event.pageX + "px")
            .style("top", d3.event.pageY + "px")
            .style("opacity", 1)
            .style("font-family", "sans-serif")
            .html(`${d.name}<br>${d.value} lines`)
    })
        .on("mouseout", function () {
            // Hide the tooltip
            tooltip.style("opacity", 0)
        })

    // Legend
    if (firstCharacterName == secondCharacterName) {
        svg.append("circle").attr("cx", width).attr("cy", 50).attr("r", 7).style("fill", color(firstCharacterName))
        svg.append("text").attr("x", width + 15).attr("y", 50).text(`${firstCharacterName}`).style("font-size", "1.3rem").attr("alignment-baseline", "middle")
    }
    else {
        svg.append("circle").attr("cx", width).attr("cy", 50).attr("r", 7).style("fill", color(firstCharacterName))
        svg.append("text").attr("x", width + 15).attr("y", 50).text(`${firstCharacterName}`).style("font-size", "1.3rem").attr("alignment-baseline", "middle")
        svg.append("circle").attr("cx", width).attr("cy", 50 + 25).attr("r", 7).style("fill", color(secondCharacterName))
        svg.append("text").attr("x", width + 15).attr("y", 50 + 25).text(`${secondCharacterName}`).style("font-size", "1.3rem").attr("alignment-baseline", "middle")
    }
}

function getLinesByCharacterData() {
    let firstCharacterName = $(`#${firstCharacterBoxId} p`).html()
    let secondCharacterName = $(`#${secondCharacterBoxId} p`).html()
    let result = []
    let i
    for (i = 1; i <= 9; i++) {
        let object = {
            season: `Season ${i}`,
        }
        object[firstCharacterName] = getNumOfLinesFromCharacterAndSeason(firstCharacterName, i)
        object[secondCharacterName] = getNumOfLinesFromCharacterAndSeason(secondCharacterName, i)
        result.push(object)
    }
    return result
}

function getNumOfLinesFromCharacterAndSeason(characterName, season) {
    return allLines.filter(function (item) {
        return item.speaker == characterName && item.season == season
    }).length
}

function selectCharacter(boxId) {
    if ($("#firstCharacter").is(":checked")) {
        $(`#${firstCharacterBoxId}`).css("border", "2px solid black")
        firstCharacterBoxId = boxId
    }
    else {
        $(`#${secondCharacterBoxId}`).css("border", "2px solid black")
        secondCharacterBoxId = boxId
    }
    $(`#${firstCharacterBoxId}`).css("border", "0.3rem solid red")
    $(`#${secondCharacterBoxId}`).css("border", "0.3rem solid blue")
    drawLinesByCharacterComparisonChart()
}


/* Functions for data manipulation */
function getSeason(seasonNumber) {
    return allEpisodes.filter(episode => episode.Season == seasonNumber)
}

function getNumberOfEpisodes(season) {
    return season.length
}

function getAverageRating(season) {
    let ratingsSum = 0
    $.each(season, function (index, episode) {
        ratingsSum += episode.Ratings
    })
    return ratingsSum / season.length
}

function getAverageViewership(season) {
    let viewershipSum = 0
    $.each(season, function (index, episode) {
        viewershipSum += episode.Viewership
    })
    return viewershipSum / season.length
}

function getTopRatedEpisodes(numberOfEpisodes) {
    if (numberOfEpisodes <= allEpisodes.length) {
        let topRatedEpisodes = allEpisodes.slice()
        topRatedEpisodes.sort(function (a, b) {
            let valueA = a.Ratings
            let valueB = b.Ratings
            if (valueA < valueB) {
                return 1
            }
            if (valueA > valueB) {
                return -1
            }
            return 0
        })
        return topRatedEpisodes.slice(0, numberOfEpisodes)
    }
    else alert("There is only " + allEpisodes.length + " episodes.")
}

function getMostViewedEpisodes(numberOfEpisodes) {
    if (numberOfEpisodes <= allEpisodes.length) {
        let mostViewedEpisodes = allEpisodes.slice()
        mostViewedEpisodes.sort(function (a, b) {
            let valueA = a.Viewership
            let valueB = b.Viewership
            if (valueA < valueB) {
                return 1
            }
            if (valueA > valueB) {
                return -1
            }
            return 0
        })
        return mostViewedEpisodes.slice(0, numberOfEpisodes)
    }
    else alert("There is only " + allEpisodes.length + " episodes.")
}

function join(t, a, s) {
    function format(m) {
        let f = new Intl.DateTimeFormat('en', m)
        return f.format(t)
    }
    return a.map(format).join(s)
}

function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value)
}

function getTotalWordsSpoken(data) {
    let sum = 0, i
    for (i = 1; i <= 9; i++) {
        sum += parseInt(data[`Season ${i}`])
    }
    return sum
}